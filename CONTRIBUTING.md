# Contributor Guidlines
First, and most importantly, thank you for your interest in working together! 

This document tells you what you can expect while working with me, and what I expect when working with you.

What you can expect from me:
- Repsonsiveness. I will respond to communications within 24 hours any weekday, or if on the weekend the following Monday. I will try to respond sooner if possible. I will make my self available whenever possible, and will try to be available for you weekends as well as weekdays.
- Availability for questions. I will gladly answer any and all questions about the job, no matter how minor. If in doubt, please ask! I would rather that you ask me, than use your time doing something that doesn't need to be done
- Vision for the work. I will provide the objective of the work to be accomplish and connect the work with the larger reason it needs to be done
- Fairness. I will work with you to evaluate how a project is going, and whether or not we need to make any changes to it. Any changes that we make should be fair to both parties. I want to pay a fair price for work, and I want work that is of a fair quality. 

What I expect from you:
- Questions. I expect you to ensure that you understand the requirements of a project. It is your responsibility to be sure of the specifications for a project. I will do my best to write clear and concise requirements, but if something seems vague, ask for clarification. 
- Quality work. I expect you to understand the point of the work, and check your work for errors before submitting it to me. If a few errors are there, do not worry! I'll simply bring them to your attention, and expect that you remedy them at your earliest convience. 
- Forward progress. I expect you to check in at least twice a week to report on how the job is going. I understand that work takes time, but I need to know how the work is going.
- Responsiveness. I expect you to respond to communications within 48 hours, or the following Monday if it is a weekend. If you'll be on vacation, or away from your computer for more than 48 hours during the week, it will be fine. Just message me before and let me know you will be unavailable.


If any job requires something that is different from the above guidelines, I will explicitly notify you, and make sure that we are in agreement. 

-Hurricane